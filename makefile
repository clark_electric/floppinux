#####################
# Floppinux Makefile
#####################

# Config Variables
ARCH              = x86
LINUX_DIR         = linux
LINUX_CFG         = $(LINUX_DIR)/.config
BUSYBOX_DIR       = busybox-1.33.1
BUSYBOX_CFG       = $(BUSYBOX_DIR)/.config
BUILDROOT_DIR     = buildroot-2021.02.2
BUILDROOT_CFG     = $(BUILDROOT_DIR)/.config
FILESYSTEM_DIR    = filesystem
MOUNT_POINT       = /mnt/disk
INITTAB           = inittab
RC                = rc
SYSLINUX_CFG      = syslinux.cfg
WELCOME           = welcome
ROOTFS_SIZE       = 1440

# Generated Files
ROOTFS        = rootfs.cpio.gz
FSIMAGE       = floppinux.img

# Recepie Files
BZIMAGE       = $(LINUX_DIR)/arch/x86/boot/bzImage
INIT          = $(FILESYSTEM_DIR)/sbin/init
TOOLCHAIN     = $(BUILDROOT_DIR)/output/host/bin/toolchain-wrapper

export PATH  := $(PWD)/$(BUILDROOT_DIR)/output/host/bin:${PATH}

floppinux: $(TOOLCHAIN) $(BZIMAGE) $(INIT) $(ROOTFS) $(FSIMAGE)
toolchain: $(TOOLCHAIN)
linux:     $(BZIMAGE)
busybox:   $(INIT)
rootfs:    $(ROOTFS)
fsimage:   $(FSIMAGE)
all:       toolchain linux busybox

$(TOOLCHAIN): $(BUILDROOT_CFG)
	cd $(BUILDROOT_DIR) && $(MAKE) toolchain

$(BZIMAGE): $(TOOLCHAIN) $(LINUX_CFG)
	echo $$PATH; cd $(LINUX_DIR); $(MAKE) ARCH=$(ARCH) CROSS_COMPILE=i486-floppinux-linux-uclibc- bzImage
	@echo Kernel size
	ls -al $(BZIMAGE)

$(INIT): $(TOOLCHAIN) $(BUSYBOX_CFG)
	echo $$PATH; cd $(BUSYBOX_DIR) && $(MAKE) CROSS_COMPILE=i486-floppinux-linux-uclibc- && $(MAKE) CROSS_COMPILE=i486-floppinux-linux-uclibc- install

$(ROOTFS): $(INIT) $(BUSYBOX_CFG) $(INITTAB) $(RC) $(WELCOME)
	sudo mkdir -pv $(FILESYSTEM_DIR)/{dev/misc,proc,etc/init.d,sys,tmp}
	- sudo mknod $(FILESYSTEM_DIR)/dev/null c 1 3
	- sudo mknod $(FILESYSTEM_DIR)/dev/ttyS0 c 4 2
	- sudo mknod $(FILESYSTEM_DIR)/dev/console c 5 1
	- sudo mknod $(FILESYSTEM_DIR)/dev/rtc0 c 249 0
	sudo cp $(INITTAB) $(FILESYSTEM_DIR)/etc/
	sudo cp $(RC) $(FILESYSTEM_DIR)/etc/init.d/
	sudo cp $(WELCOME) $(FILESYSTEM_DIR)/
	sudo chmod +x $(FILESYSTEM_DIR)/etc/init.d/rc
	sudo chown -R root:root $(FILESYSTEM_DIR)
	@echo $(ROOTFS) size:
	cd $(FILESYSTEM_DIR); find . | cpio -H newc -o | gzip -9 > ../rootfs.cpio.gz

$(FSIMAGE): $(ROOTFS) $(BZIMAGE) $(INITTAB) $(RC) $(SYSLINUX_CFG) $(WELCOME)
	dd if=/dev/random of=$(FSIMAGE) bs=1k count=$(ROOTFS_SIZE)
	mkdosfs $(FSIMAGE)
	syslinux --install $(FSIMAGE)
	sudo mount -o loop $(FSIMAGE) $(MOUNT_POINT)
	sudo cp $(SYSLINUX_CFG) $(BZIMAGE) $(ROOTFS) $(MOUNT_POINT)
	sync
	sudo umount $(MOUNT_POINT)

.PHONY: clean size clean_busybox clean_linux clean_buildroot clean_filesystem

size:
	sudo mount -o loop $(FSIMAGE) $(MOUNT_POINT)
	df -h /mnt/disk
	ls -alh /mnt/disk
	sudo umount $(MOUNT_POINT)

clean: clean_linux clean_busybox clean_filesystem

clean_linux:
	cd $(LINUX_DIR) && $(MAKE) clean

clean_busybox:
	cd $(BUSYBOX_DIR) && $(MAKE) clean

clean_filesystem:
	sudo rm -rvf $(FILESYSTEM_DIR)
	rm -vf $(FSIMAGE) $(ROOTFS)

clean_buildroot:
	cd $(BUILDROOT_DIR) && $(MAKE) clean
